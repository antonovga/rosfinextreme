class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.text :raw_data, null: false
      t.integer :ru_fl_number
      t.string :full_name, null: false
      t.string :full_name_alt
      t.date :born_at
      t.string :birth_place

      t.timestamps null: false
    end
  end
end
