class PageParser
  PAGE_URL = 'http://fedsfm.ru/documents/terrorists-catalog-portal-act'
  CSS_RULE = '#textRussianFL > p'

  def initialize
    @document = nil
  end

  def parse(url = nil)
    remote_url = url || PAGE_URL
    begin
      @document = Nokogiri::HTML(open(remote_url))
    rescue
      Rails.logger.error "could not open #{ remote_url }"
    end
    return unless @document.present?

    people_list = @document.css(CSS_RULE) || nil

    if people_list.blank?
      Rails.logger.error "no people found with css rule: #{ CSS_RULE }"
      return
    end

    people_array = people_list.map do |person_record|
      Person.new clean_values(person_record)
    end

    Person.transaction do
      Person.delete_all
      Person.import people_array
    end
  end

  private

  # С форматом всё плохо, первая ревизия от 27 мая 2016, сделан запрос в Росфин по поводу этого говна
  # единственное надёжное - первый параметр до запятой это номер плюс имя
  # год рождения может быть, а может не быть, место плавает
  # место рождения может быть, а может не быть, всегда последний параметр
  # есть ещё волшебный - параметр альтернативное имя, обычно, идет вторым параметром, если вообще есть
  # В ИТОГЕ - поиск по полю raw_data
  def clean_values(raw_record)
    raw_data = raw_record.text
    raw_params = raw_data.split(',').map(&:strip)
    remote_id, full_name = raw_params.first.split('.').map(&:strip)
    full_name.chomp!('*')

    full_name_alt = raw_data.match(/(?<=\().+?(?=\))/)&.to_s

    birth_date = raw_data.match(/(\d{2}.\d{2}.\d{4})/)&.to_s
    birth_date = Date.parse(birth_date) if birth_date

    birth_place = raw_params.count > 2 ? raw_params.last&.downcase.chomp(';') : nil

    { raw_data: raw_data,
      ru_fl_number: remote_id&.to_i,
      full_name: full_name,
      full_name_alt: full_name_alt,
      birth_place: birth_place,
      born_at: birth_date
    }
  end
end