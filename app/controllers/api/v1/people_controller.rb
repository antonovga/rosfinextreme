module Api
  module V1
    class PeopleController < Api::BaseController
      def index
        @people = Person.all || []
      end

      def in_list
        @result = { inlcusion: nil, people: [] }

        if params[:full_name].present?
          full_name = params[:full_name].mb_chars.upcase.to_s

          @result[:people] = Person.where('full_name = ? OR full_name_alt = ?', full_name, full_name) || []
          @result[:inclusion] = @result[:people].present?
        end
      end

      def search
        @people = params[:query].present? ? Person.where('raw_data ILIKE ?', "%#{ params[:query] }%") : []
      end
    end
  end
end
