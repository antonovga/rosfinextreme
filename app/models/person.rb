class Person < ActiveRecord::Base
  validates :raw_data, :full_name, presence: true
end
