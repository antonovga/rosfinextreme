$(document).ready ->
  $('#search').selectize({
    labelField: 'full_name',
    valueField: 'full_name',
    searchField: 'raw_data',
    create: false,
    render:
      option: (item, escape) ->
        item_string = '<div><span class="title">'
        item_string += ('<span class="name">' + escape(item.full_name) + '</span>')
        item_string += ('<span class="name-alt">(' + escape(item.full_name_alt) + ')</span>') if item.full_name_alt
        item_string += ('<span class="born-at">' + escape(item.born_at) + '</span>') if item.born_at
        item_string += '</span>'
        item_string +=('<span class="birth-place">' + escape(item.birth_place) + '</span>') if item.birth_place
        item_string += '</div>'
        return item_string
    ,
    load: (query, callback) ->
        return callback() if (!query.length)
        $.ajax
          url: '/api/v1/people/search'
          type: 'GET'
          dataType: 'json'
          data: { query: query }
          error: () ->
            callback()
          success: (res) ->
            callback(res)
  })