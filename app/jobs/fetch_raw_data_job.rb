class FetchRawDataJob < ActiveJob::Base
  queue_as :fetch_raw_data

  def perform
    PageParser.new.parse
  end
end
