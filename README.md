## README

### Ruby version
2.3.1

`cat .ruby-version`

`cat .ruby-gemset`

### System dependencies
none


### Configuration

`bundle install`

`cp ./config/database.yml.skel ./config/database.yml`

`cp ./config/secrets.yml.skel ./config/secrets.yml`

### Database creation

`rake db:create`

### Database initialization

`rake db:migrate`

### How to run the test suite

`rake test`

### Services (job queues, cache servers, search engines, etc.)

#### sidekiq

config - `cat ./config/sidekiq.yml`

#### cron

`whenever -w`

config - `cat config/schedule.rb`