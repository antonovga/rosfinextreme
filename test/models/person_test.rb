require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  test 'should not save person without raw_data & full_name' do
    person = Person.new
    assert_not person.save

    person.assign_attributes full_name: 'Иванов', raw_data: nil
    assert_not person.save

    person.assign_attributes full_name: nil, raw_data: 'raw data'
    assert_not person.save

    person.assign_attributes full_name: 'Иванов', raw_data: 'raw data'
    assert person.save
  end
end
