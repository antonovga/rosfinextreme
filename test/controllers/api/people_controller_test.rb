require 'test_helper'

class Api::V1::PeopleControllerTest < ActionController::TestCase
  test 'should get index' do
    get :index, format: :json

    assert_response :success
  end

  test 'should get in_list' do
    get :in_list, format: :json
    assert_response :success
  end

  test 'in_list should return empty result if no param given' do
    get :in_list, format: :json
    answer = JSON.parse(response.body)

    assert_equal answer['in_list'], nil
    assert_equal answer['people'], []
  end

  test 'should get search' do
    get :search, format: :json

    assert_response :success
  end

  test 'search should return empty result if no param given' do
    get :search, format: :json
    answer = JSON.parse(response.body)

    assert_equal answer, []
  end
end
