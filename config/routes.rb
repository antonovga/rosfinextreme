Rails.application.routes.draw do
  get 'pages/home'
  get 'people/search', defaults: { format: :json }

  namespace :api do
    namespace :v1, defaults: { format: :json } do
      get 'people/search'
      get 'people', to: 'people#index'
      get 'people/in_list'
    end
  end

  root 'pages#home'
end
