require 'csv'

namespace :csv do
  namespace :people do
    desc 'export all people to a csv file'
    task :all => :environment do
      CSV.open('./people.csv', 'wb') do |csv|
        csv << Person.attribute_names
        Person.all.each do |person|
          csv << person.attributes.values
        end
      end
    end
  end
end
